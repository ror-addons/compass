<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
 
    <UiMod name="compass" version="2.0.0" date="14/01/2009" >
        <Author name="gutgut" email="nixui@spambog.com" />
        <Description text="compass" />
        <VersionSettings gameVersion="1.3.4" windowsVersion="1.0" savedVariablesVersion="1.0" /> 
        
		<Files>
            <File name="compass.lua" />
            <File name="compass.xml" />
            <File name="compass.dds" />
        </Files>
        
        <OnInitialize>
            <CreateWindow name="compasselement" show="true" />
            <CreateWindow name="compassbg" show="true" />
            <CallFunction name="compass.Initialize" />
        </OnInitialize>

          
    </UiMod>
 
</ModuleFile>

