
compass = {}
compass.radius = 58
compass.vradius = 11
compass.verschiebung = -1.5
compass.olddir={1,0,0}
compass.ebene = 0

local levent = false

compass.rose={
{v=".",p=22.5,t=3}, --2
{v=".",p=67.5,t=3},  --4
{v=".",p=112.5,t=3},--6
{v=".",p=157.5,t=3},--8
{v=".",p=202.5,t=3},--10
{v=".",p=247.5,t=3},--12
{v=".",p=292.5,t=3},--14
{v=".",p=337.5,t=3},
{v="S",p=0,t=1},    --1
{v="se",p=45,t=2},  --3 
{v="E",p=90,t=1},   --5
{v="ne",p=135,t=2}, --7 
{v="N",p=180,t=1},  --9
{v="nw",p=225,t=2}, --11 
{v="W",p=270,t=1},  --13
{v="sw",p=315,t=2} --15  
}--16


compass.sort= {

{lr=330,rr=30,xscale = 1.00, xalpha = 0.80, xr = 180, xg = 180, xb = 180, ly=2},

{lr=300,rr=60,xscale = 0.93, xalpha = 0.85, xr = 195, xg = 195, xb = 195, ly=2},

{lr=270,rr=90,xscale = 0.86, xalpha = 0.90, xr = 210, xg = 210, xb = 210, ly=2},

{lr=240,rr=120,xscale = 0.79, xalpha = 0.95, xr = 225, xg = 225, xb = 225, ly=1},

{lr=200,rr=160,xscale = 0.72, xalpha = 1.0, xr = 240, xg = 240, xb = 120, ly=1},

{lr=179,rr=180,xscale = 0.65, xalpha = 1.0, xr = 255, xg = 255, xb = 0, ly=1},

}


function compass.Initialize ()

     for k,v in ipairs(compass.rose) do
        win="cmp"..tostring(k)
        CreateWindowFromTemplate(win, "compasselement", "compassbg")
        LabelSetText(win.."text", L""..towstring(v.v));
        WindowSetShowing(win,true)
     end
  
        BroadcastEvent( SystemData.Events.PLAYER_POSITION_UPDATED )

        LayoutEditor.RegisterWindow("compassbg",L"compass",L"compass",true,true,true,nil )
        
        if not levent then
            RegisterEventHandler (SystemData.Events.PLAYER_POSITION_UPDATED, "compass.OnPlayerPositionUpdated")
        end
        WindowSetShowing("compassbg",true)
        WindowSetLayer("compassbg", compass.ebene )
        WindowSetAlpha("compassbg", 1)  

        compass.draw()
    
end

function getsort(dircc)
   for k1,v1 in ipairs(compass.sort) do
          if dircc > v1.lr or dircc < v1.rr then
            v1.index = k1
            return v1
          end
    end
end


function compass.draw(dir)

    if not dir then dir = 0 end
    
    for k,v in ipairs(compass.rose) do

        win="cmp"..k
        wintext = win.."text"
        
        local dircc = v.p+dir
        
        if dircc > 359 then dircc = dircc - 360 end

        local sortval = getsort(dircc)

        dircc = dircc+compass.verschiebung
        local uiScale = InterfaceCore.GetScale()       
        local windowScale = WindowGetScale( "compassbg" )   
        
        local xscale   = sortval.xscale*windowScale ---*uiscale
        
        
         WindowSetScale(win, xscale)
         WindowSetFontAlpha(wintext, sortval.xalpha)
         LabelSetTextColor(wintext,sortval.xr,sortval.xg,sortval.xb)
         
        WindowSetLayer(win, compass.ebene+sortval.ly )
        
        
                --LabelSetText(wintext, L""..towstring(dircc));
                --LabelSetText(wintext, L""..towstring(sortval.index));
                
        dircc = math.rad(dircc)
        
        local vx = math.sin(dircc) * compass.radius 
        local vy = math.cos(dircc) * compass.vradius
        
        WindowClearAnchors(win)
        WindowAddAnchor(win, "center", "compassbg", "center", vx,vy ) 
        
        WindowSetShowing(win,true)
 
    end

        compass.olddir[3]=compass.olddir[2]
        compass.olddir[2]=compass.olddir[1]
        compass.olddir[1] = dir

end


local playerPosition = {}

function compass.OnPlayerPositionUpdated(worldX, worldY)

    --parts of the function from Bloodwalker ;)
    if levent == false then
        levent = true
    else
    
    local newPos = {
        worldX = worldX,
        worldY = worldY,
    }
     
		local fromX = playerPosition.worldX or 0
		local fromY = playerPosition.worldY or 0
		local toX, toY = newPos.worldX, newPos.worldY
		local angle = math.deg( math.atan2( (toY - fromY), (toX - fromX) ) )
        angle = angle + 90
		if angle < 0 then
			angle = angle + 360
		end

		playerDirection = tonumber(string.format("%.0f", angle))
        playerPosition = newPos

    
    if compass.olddir[1] ~= playerDirection and compass.olddir[2] ~= playerDirection and compass.olddir[3] ~= playerDirection then
        compass.draw(playerDirection) 
    end
    
    end
    
end